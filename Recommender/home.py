import pandas as pd
df = pd.read_csv("Book1.csv")
print(" \t\t\tBridge.it People\t\t\t\t\n\n")
def showData():
    print(df)
def showMembers():
    for index, member in enumerate((df['User Names'])):
        index += 1
        print("\t\t\t", index, member)
def showColumns():
    print(df.columns)

def showOrganizations():
    for index, organization in enumerate(df['OrganizationSize'].unique()):
        print(index, organization)
def fetchNodes(value):
    return df[(df['LookingtoFind']) == df.loc[value]['LookingtoShare']]
def Recommender(df2 , organizationSize):
    df_by_organization = df2[(df2['OrganizationSize']) == organizationSize]
    print(str(len(df_by_organization)) + " are ogranizations with chooezen size ")
    df3 = df_by_organization.sort_values(['Distance'])
    return df3
showData()
profile = input("Choose your Profile")
try:
    val = int(profile)
    print(str(val))
    if(val>len(df)):
        print(" This profile does not exist...")
    else:
        print("Welcome ", df.loc[val]['User Names'])
        print("1.   Share")
        print("2.   Find")
        purpose = input("You are here for ? ")
        if(int(purpose)==1 or int(purpose)==2): # continue
            if(int(purpose)==1): # SHARE
                print("Share")
                showOrganizations()
                size = input("Choose Organization size")
                if (int(size) == 0 or int(size) == 1 or int(size) == 2):
                    organizationSize = df['OrganizationSize'].unique()[int(size)]
                    print("Organization Size is choosen", organizationSize)
                    df2 = fetchNodes(val)
                    if (len(df2) > 0):
                        print(
                            "Following Nodes are Looking for  same thing. You can contact them to share your materials : ")
                        print(df2)
                        print("According to Distance and Organization Size you have choosen , These people are best to contact")
                        df3 = Recommender(df2 , organizationSize)
                        print(df3.head())
            elif(int(purpose)==2): # LOOKING
                print("Find")
                showOrganizations()
                size = input("Choose Organization size")
                organizationSize = df['OrganizationSize'].unique()[int(size)]
                if (int(size) == 0 or int(size) == 1 or int(size) == 2):
                     print("Organization Size is chosen", organizationSize)
                df2 = fetchNodes(val)
                if (len(df2) > 0):
                    print(
                        "Following People are SHARING  same thing. You can contact them to GET YOUR MATERIALS : ")
                    print(df2)

                    print("According to Distance and Organization Size you have choosen , These people are best to contact")
                    df3 = Recommender(df2, organizationSize)
                    print(df3.head())
        else:
            print("Wrong input ")
except  ValueError:
    print( str(ValueError))

