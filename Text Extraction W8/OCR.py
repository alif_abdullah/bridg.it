import pytesseract
from PIL import Image
import nltk
nltk.download('punkt')
from nltk.corpus import stopwords
import boto3
import csv
import speech_recognition as sr
from nltk.stem import PorterStemmer
ps = PorterStemmer()

stop_words = stopwords.words('english')
def ImageToText():
    try:

        pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
        img = Image.open('google.jpg')
        text = pytesseract.image_to_string(img)
        print(" ---------Data has been extracted ------ ")
        print(text)
        print("--------------------------------------")
        print(" Tokenizing Now")
        tokenize= nltk.word_tokenize(text)
        print(tokenize)
        meaning_ful_workds = [word for word in tokenize if word not in stop_words]
        print(" After removing stop Words ")
        print(meaning_ful_workds)
        return meaning_ful_workds
    except Exception as e:
        print(e)
def ImageLabelling():
    with open('credentials.csv', 'r') as input:
        next(input)
        reader = csv.reader(input)
        for line in reader:
            access_key_id = line[2]
            secret_access_key  = line[3]

    photo = 'persons.jpg'
    client = boto3.client('rekognition',
                          region_name='us-west-2',
                          aws_access_key_id = access_key_id,
                          aws_secret_access_key = secret_access_key)
    with open(photo , 'rb') as source_image:
        source_bytes = source_image.read()

    response = client.detect_labels(Image = {'Bytes' : source_bytes},
                                    MaxLabels = 7)
    print(response)

def PersoanlityDetection():
    with open('credentials.csv', 'r') as input:
        next(input)
        reader = csv.reader(input)
        for line in reader:
            access_key_id = line[2]
            secret_access_key  = line[3]

    photo = 'sundar.jpg'
    client = boto3.client('rekognition',
                          region_name='us-west-2',
                          aws_access_key_id = access_key_id,
                          aws_secret_access_key = secret_access_key)
    with open(photo , 'rb') as source_image:
        source_bytes = source_image.read()

    response = client.recognize_celebrities(Image = {'Bytes' : source_bytes},)
    print(response)

def VideoToText():
    import subprocess
    import ffmpeg
    command = "ffmpeg -i video.mp4 -ab 160k -ac 2 -ar 44100 -vn audio1.wav"

    subprocess.call(command, shell=True)
    r = sr.Recognizer()
    with sr.AudioFile("audio1.wav") as source:
        audio = r.record(source)  # read the entire audio file
        videoText = (r.recognize_google(audio))
    print("--------------------------------------")
    print(" Tokenizing Now")
    tokenize = nltk.word_tokenize(videoText)
    print(tokenize)
    meaning_ful_workds = [word for word in tokenize if word not in stop_words]
    print(" After removing stop Words ")
    print(meaning_ful_workds)
    return meaning_ful_workds

def ImageModeration():
    with open('credentials.csv', 'r') as input:
        next(input)
        reader = csv.reader(input)
        for line in reader:
            access_key_id = line[2]
            secret_access_key  = line[3]

    photo = 'adult.png'
    client = boto3.client('rekognition',
                          region_name='us-west-2',
                          aws_access_key_id = access_key_id,
                          aws_secret_access_key = secret_access_key)
    with open(photo , 'rb') as source_image:
        source_bytes = source_image.read()

    response = client.detect_moderation_labels(Image = {'Bytes' : source_bytes},)
    print(response)


def Prediction():
    from difflib import SequenceMatcher
    text1 = ImageToText()
    text2 = VideoToText()
    m = SequenceMatcher(None, text1, text2)
    print("Similarity : " , m.ratio())